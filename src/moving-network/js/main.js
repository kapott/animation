import Particle from './Particle.js'
import CollisionVisitor from './CollisionVisitor.js'

const visitor = new CollisionVisitor()
const canvas = document.getElementById('canvas')
const ctx = canvas.getContext('2d')
canvas.width = window.innerWidth
canvas.height = window.innerHeight

let particlesArray;

let mouse = {
    x: null,
    y: null,
    radius: (canvas.height/180) * (canvas.width/180)
}

window.addEventListener('mousemove', (event) => {
    mouse.x = event.x
    mouse.y = event.y
})

window.addEventListener('resize', (event) => {
    canvas.width = window.innerWidth
    canvas.height = window.innerHeight
    ctx.clearRect(0,0,window.innerWidth, window.innerHeight)
    mouse.radius = ((canvas.height / 180) * (canvas.width/180))
    init()
})

window.addEventListener('click', (e) => {
    let size = (Math.random() * 2) + 1
    let x = e.x
    let y = e.y
    let directionX = (Math.random() * 10) / 20
    let directionY = (Math.random() * 10) / 20
    const nodeColor = 'rgba(0,0,0,0.75)'
    particlesArray.push(new Particle(ctx, x, y, directionX, directionY, size, nodeColor))
})

function init () {
    window.particles = particlesArray = []
    let numberOfParticles = (canvas.height * canvas.width) / 7500
    const nodeColor = 'rgba(0,0,0,0.75)'

    for (let i = 0; i < numberOfParticles; i++) {
        let size = (Math.random() * 2) + 1
        let x = (Math.random() * ((innerWidth - size * 2) - (size * 2)) + size * 2)
        let y = (Math.random() * ((innerHeight - size * 2) - (size * 2)) + size * 2)
        let directionX = (Math.random() * 10) / 20
        let directionY = (Math.random() * 10) / 20
        particlesArray.push(new Particle(ctx, x, y, directionX, directionY, size, nodeColor))
    }
}

function connect () {
    let opacity;
    for (let a = 0; a < particlesArray.length; a+=1) {
        for (let b = a; b < particlesArray.length; b+=1) {
            let distance =
                ((particlesArray[a].x - particlesArray[b].x) * (particlesArray[a].x - particlesArray[b].x))
                +((particlesArray[a].y - particlesArray[b].y) * (particlesArray[a].y - particlesArray[b].y))
            
                if (distance < (canvas.width / 10) * (canvas.height/10)) {
                    opacity = 1 - (distance / 50000)
                    ctx.strokeStyle='rgba(0,0,0,'+ opacity +')'
                    ctx.lineWidth = (particlesArray[a].size + particlesArray[b].size) / 8
                    ctx.beginPath()
                    ctx.moveTo(particlesArray[a].x, particlesArray[a].y)
                    ctx.lineTo(particlesArray[b].x, particlesArray[b].y)
                    ctx.stroke()
                }
        }
    }
}

function animate () {
    requestAnimationFrame(animate)
    ctx.clearRect(0,0,innerWidth, innerHeight)

    connect()
    for (let i = 0; i < particlesArray.length; i++) {        
        particlesArray[i].update({canvas, mouse})
    }
    
}

init()
animate()
