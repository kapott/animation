export default class Particle {
    constructor(context, x, y, directionX, directionY, size, color) {
        this.ctx = context
        this.x = x
        this.y = y
        this.directionX = directionX
        this.directionY = directionY
        this.size = size
        this.originalSize = size
        this.color = color
        this.shrink = Math.random() >= 0.5
    }

    draw () {
        this.ctx.beginPath();
        this.ctx.arc(this.x, this.y, this.size, Math.PI * 2, false)
        this.ctx.fillStyle = this.color //'#ff006a'
        this.ctx.fill()
    }

    update ({canvas, mouse}) {
        if (this.x > canvas.width || this.x < 0) {
            this.directionX = -this.directionX
        }
        if (this.y > canvas.height || this.y < 0) {
            this.directionY = -this.directionY
        }

        //collision detection
        let dx = mouse.x - this.x;
        let dy = mouse.y - this.y;
        let distance = Math.sqrt(dx*dx + dy*dy)
        if (distance < mouse.radius + this.size) {
            if (mouse.x < this.x && this.x < canvas.width - this.size * 10) {
                this.x += 10
                this.y += 10
            }
            if (mouse.x > this.x && this.x > this.size * 10 ){
                this.x -= 10
                this.y -= 10
            }
            if (mouse.y < this.y && this.y < canvas.height - this.size * 10) {
                this.y += 10
                this.y -= 10
            }
            if (mouse.y > this.y && this.y > this.size * 10) {
                this.y -= 10
                this.y -= 10
            }
            this.directionX = -this.directionX
            this.directionY = -this.directionY
        }
        this.x += this.directionX
        this.y += this.directionY
        
        this.shrinkOrGrow()
        this.draw()
    }

    shrinkOrGrow () {
        const step = 0.05
        const minSize = 0.2 * this.originalSize
        const maxSize = 2 * this.originalSize

        if (this.shrink && this.size - step < minSize) {
            this.shrink = false
        }

        if (!this.shrink && this.size >= maxSize) {
            this.shrink = true
        }

        if (this.shrink && this.size - step > minSize) {
            this.size -= step
        } else {
            this.size += step
        }
    }

    // Visitor pattern so we can achieve O(N) collision detection among eachother
    accept (visitor) {
        visitor.visit(this)
    }
}